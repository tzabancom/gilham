<div class="swiper-slide">
	<div class="inner clearfix">
		<div class="bg-image" style="background-image:url('img/slide-3.png')"></div>
		<div class="container clearfix">
			
			<div class="right-part">
				<div class="stamp">
					<img src="img/stamp3.png" alt="">
				</div>
				<div class="text white">
					<p >Overlooking the<br/>
					Mediterranean in<br/>
					the pine clad<br/>
					mountains of<br/>
					Cyprus, refresh<br/>
					yourself and your<br/>
					senses at our<br/>
					boutique<br/>
					wine hotel
					</p>
				</div>
			</div>
			<div class="left-part">
				<?php include 'top-text.php'; ?>
				<div class="title-wraper">
					<h1><span>GILLHAM</span>
	 					<span>VINEYARD</span>
	 					<span>HOTEL</span>
	 				</h1>
 				</div>
				<div class="mobile">
					<div class="stamp">
						<img src="img/stamp3.png" alt="">
					</div>
					<div class="text white">
						<p >Overlooking the
						Mediterranean in
						the pine clad
						mountains of
						Cyprus, refresh
						yourself and your
						senses at our
						boutique
						wine hotel
						</p>
					</div>
				</div>
			</div>
			<?php include 'bottom-text.php'; ?>
		</div>
	</div>
</div>