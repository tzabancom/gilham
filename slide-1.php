<div class="swiper-slide">
	<div class="inner clearfix">
		<div class="bg-image" style="background-image:url('img/slide-1.png')"></div>
		<div class="container clearfix">
			
			<div class="right-part">
				<div class="stamp">
					<img src="img/stamp.png" alt="">
				</div>
				<div class="text grey">
					<p >Listen to your<br/>
					soul. Discover<br/>
					the culture<br/>
					of wine at<br/>
					the Gillham <br/>
					estate, where<br/>
					history meets<br/>
					winemaking’s<br/>
					new world</p>
				</div>
			</div>
			<div class="left-part">
				<?php include 'top-text.php'; ?>
				<div class="title-wraper">
					<h1><span>gillham’s</span>
	 					<span>wine</span>
						<span>culture</span>
					</h1>
				</div>

				<div class="mobile">
					<div class="stamp">
						<img src="img/stamp.png" alt="">
					</div>
					<div class="text grey">
						<p >Listen to your
						soul. Discover
						the culture
						of wine at
						the Gillham
						estate, where
						history meets
						winemaking’s
						new world</p>
					</div>
				</div>
				
			</div>
			<?php include 'bottom-text.php'; ?>
		</div>
	</div>
</div>