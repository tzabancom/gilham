<?php include 'head.php'; ?>


<div class="slider swiper-container">
	<div class="swiper-wrapper">
		<?php include 'slide-3.php'; ?>
		<?php include 'slide-1.php'; ?>
        <?php include 'slide-2.php'; ?>

	</div>
	<div class="controls">
		<div class="container clearfix">
			<div class="swiper-button playing">
				
			</div>
			<div class="swiper-pagination"></div>
		</div>

	</div>
</div>


<?php include 'footer.php'; ?>

