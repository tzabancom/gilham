<div class="swiper-slide">
	<div class="inner clearfix">
		<div class="bg-image" style="background-image:url('img/slide-2.png')"></div>
		<div class="container clearfix">
			
			<div class="right-part">
				<div class="stamp">
					<img src="img/stamp2.png" alt="">
				</div>
				<div class="text white">
					<p >On our boutique<br/>
						estate, artisan<br/>
						vintners are<br/>
						bringing A new<br/>
						chapter to the<br/>
						island, using<br/>
						their passion<br/>
						and knowledge<br/>
						to create<br/> 
						distinguished wines</p>
				</div>
			</div>
			<div class="left-part">
				<?php include 'top-text.php'; ?>
				<div class="title-wraper">
					<h1><span>etel</span>
						<span>winery</span>  
					</h1>
				</div>

				<div class="mobile">
					<div class="stamp">
						<img src="img/stamp2.png" alt="">
					</div>
					<div class="text white">
						<p >On our boutique
							estate, artisan
							vintners are
							bringing A new
							chapter to the
							island, using
							their passion
							and knowledge
							to create
							distinguished wines</p>
					</div>
				</div>
				
			</div>
			<?php include 'bottom-text.php'; ?>
		</div>
	</div>
</div>