function allHeight(){
	var winHeight = $(window).innerHeight();
	$('.swiper-wrapper, .swiper-slide , .swiper-container').css('min-height' , winHeight );
}


$(document).ready(function(){
	var mainSlider = new Swiper('.swiper-container', {
	    speed: 1000,
	    keyboardControl:true,
	    effect: 'fade',
	    autoplay : 10000,
	    loop:false,
	    pagination: '.swiper-pagination',
	    paginationClickable: true,
	    onInit: function (swiper){
	    	var activeSlideTitle = $('.swiper-slide').eq(swiper.activeIndex).find('h1 span');
	    	var activeSlideText = $('.swiper-slide').eq(swiper.activeIndex).find('.right-part');
        	$.each(activeSlideTitle, function( index, value ) {
			  TweenMax.staggerFrom($(this) ,2, {opacity:0, x:'-=1500px', delay:0.2 * index, ease:Elastic.easeOut.config(0.3, 0.3), force3D:true}, 0.5);
			});
			TweenMax.staggerFrom($(activeSlideText) ,3, {opacity:0, delay:0.2, ease:Elastic.easeOut.config(0.3, 0.3), force3D:true}, 0.5);
	    },
        onSlideChangeStart: function (swiper){

        	var activeSlideTitle = $('.swiper-slide').eq(swiper.activeIndex).find('h1 span');
        	//var prevSlideTitle = $('.swiper-slide').eq(swiper.previousIndex).find('h1 span');

        	var activeSlideText = $('.swiper-slide').eq(swiper.activeIndex).find('.right-part');
        	//var prevSlideText = $('.swiper-slide').eq(swiper.previousIndex).find('.right-part');

   //      	$.each(prevSlideTitle, function( index, value ) {
			//   TweenMax.staggerTo($(this) ,2, {opacity:0, x:'-=1500px', delay:0.2 * index, ease:Elastic.easeOut.config(0.3, 0.3), force3D:true}, 0.5);
			// });

        	$.each(activeSlideTitle, function( index, value ) {
			  TweenMax.staggerFrom($(this) ,2, {opacity:0, x:'-=1500px', delay:0.2 * index, ease:Power1.easeOut, force3D:true}, 0.5);
			});

			//TweenMax.staggerTo($(prevSlideText) ,5, {opacity:0, y:'1500px', delay:0.2, ease:Elastic.easeOut.config(0.3, 0.3), force3D:true}, 0.5);
			TweenMax.staggerFrom($(activeSlideText) ,3, {opacity:0, delay:0.2, ease:Power1.easeOut, force3D:true}, 0.5);


        },        	    
	});

	$('.swiper-button').on('click' , function(){
		if(!$(this).hasClass('playing')){
			mainSlider.startAutoplay();
			$(this).addClass('playing');
		}else{
			mainSlider.stopAutoplay();
			$(this).removeClass('playing');
		}
	});
	allHeight();
});

$(window).resize(function(){
	allHeight();
});
